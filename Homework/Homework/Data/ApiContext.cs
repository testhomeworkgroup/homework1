﻿using Microsoft.EntityFrameworkCore;
using Homework.Models;

namespace Homework.Data
{
    public class ApiContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public ApiContext(DbContextOptions<ApiContext> options) : base(options)
        { 

        }
    }
}
