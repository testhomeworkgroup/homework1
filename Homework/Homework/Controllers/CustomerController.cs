﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Homework.Models;
using Homework.Data;

namespace Homework.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly ApiContext _context;

        public CustomerController(ApiContext contex)
        {
            _context = contex;
        }

        // Create, Edit
        [HttpPost]
        [Route("CreateOrUpdate")]
        public JsonResult CreateEditCustomer(Customer customer) 
        {
            if (customer.ID == 0)
            {
                _context.Customers.Add(customer);
            }
            else 
            {
                var customerInDb = _context.Customers.Find(customer.ID);
                if (customerInDb == null) 
                    return new JsonResult(NotFound());
                customerInDb = customer;
            }
            _context.SaveChanges();
            return new JsonResult(Ok(customer));
        }

        [HttpGet]
        [Route("GetAll")]
        public JsonResult GetAllCustomers()
        {
            var customers = _context.Customers.ToList();
            if (customers?.Count > 0)
            {
                return new JsonResult(Ok(customers));
            }
            return new JsonResult(NotFound());
        }

        [HttpGet]
        [Route("GetById")]
        public JsonResult GetCustomerById(int customerId)
        {
            var customer = _context.Customers.Find(customerId);
            if (customer != null)
            {
                return new JsonResult(Ok(customer));
            }
            return new JsonResult(NotFound());
        }
    }
}
