﻿using Homework.Data;
using Homework.Models;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestPlatform.TestHost;
using Newtonsoft.Json;
using System.Net.Http.Json;

namespace HomeworkApiTest
{
    public class CustomerTest
    {
        public readonly string url = "https://localhost:44332/api/Customer";

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public async Task CreateNewCustomer()
        {
            await using var application = new WebApplicationFactory<Program>();

            var client = application.CreateClient();

            Customer testCustomer = new Customer { ID = 1, Email = "test@test.com", FirstName = "testName", Surname = "testSurname", Password = "testPassword" };

            var activity = await client.PostAsJsonAsync(url + "/CreateOrUpdate", testCustomer);

            Assert.IsTrue(activity.IsSuccessStatusCode);
        }

        [Test]
        public async Task UpdateCustomer()
        {
            using var application = new WebApplicationFactory<Program>();

            var client = application.CreateClient();

            Customer newCustomer = new Customer { ID = 1, Email = "newTest@newTest.com", FirstName = "testName", Surname = "testSurname", Password = "testPassword" };

            var activity = await client.PostAsJsonAsync(url + "/CreateOrUpdate", newCustomer);

            Assert.IsTrue(activity.IsSuccessStatusCode);
        }

        [Test]
        public async Task GetAllCustomers()
        {
            using var application = new WebApplicationFactory<Program>();

            var client = application.CreateClient();

            var activity = await client.GetAsync(url + "/GetAll");

            Assert.IsTrue(activity.IsSuccessStatusCode);
        }
    }
}
